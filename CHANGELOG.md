## [1.0.0-alpha.16](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.15...v1.0.0-alpha.16) (2024-02-18)


### ⚠ BREAKING CHANGES

* cli-builder-generator has been renamed to go-builder-generator

### Chores

* **builders:** update go generate command ([21a6c92](https://gitlab.com/gotools-land/go-builder-generator/commit/21a6c92196b4b96d4bb23aa0cc7b760a05934071))
* **ci:** reuse goreleaser ([609adac](https://gitlab.com/gotools-land/go-builder-generator/commit/609adac0d0b8b0eeb66f5217155d500a7a9c7074))
* **golangci:** remove fieldalignment lint ([a8e5e17](https://gitlab.com/gotools-land/go-builder-generator/commit/a8e5e178bf2dec28a039d6b080fa444d3178b42c))


### Code Refactoring

* rename main executable to remove cli- prefix ([c12b8bc](https://gitlab.com/gotools-land/go-builder-generator/commit/c12b8bc49e35f86998a3b350926b7e17fb2e77d2))

## [1.0.0-alpha.15](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.14...v1.0.0-alpha.15) (2024-02-18)


### Features

* **builder:** add Copy function to builders ([b1dde83](https://gitlab.com/gotools-land/go-builder-generator/commit/b1dde83739a08a65092f5fe1c62404437d91cb7e))
* **interface:** handle empty interfaces in oneline (better visual) ([12895bc](https://gitlab.com/gotools-land/go-builder-generator/commit/12895bcdf302565284b00dc4720ea2d33d59a32e))

## [1.0.0-alpha.14](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.13...v1.0.0-alpha.14) (2024-02-17)


### ⚠ BREAKING CHANGES

* **generate:** validator is no longer automatically added to builders Build function

### Features

* **generate:** add --use-validator option to add validator usage with validate tag presence ([35f07ed](https://gitlab.com/gotools-land/go-builder-generator/commit/35f07edd77b7aa3ecbf2439c2241c93ad0f06652))


### Bug Fixes

* **struct:** handle empty structs in oneline (better visual) ([9355f13](https://gitlab.com/gotools-land/go-builder-generator/commit/9355f1315237b42ecda215f181927430d719677c))
* **struct:** linting issue following empty struct addition ([c5b4acc](https://gitlab.com/gotools-land/go-builder-generator/commit/c5b4acceb57e76e80bb20e1a829d5a546dbf8d68))
* **struct:** missing no fields addition in unit tests ([4ef7812](https://gitlab.com/gotools-land/go-builder-generator/commit/4ef78122543ac54f8833a867f332050449266c9b))


### Chores

* regenerate project layout ([15b679c](https://gitlab.com/gotools-land/go-builder-generator/commit/15b679cff7e0212dde1a029874f1459043560124))
* tidying ([833eb80](https://gitlab.com/gotools-land/go-builder-generator/commit/833eb80b5c244ecefa72032143d8e7d7231a0d90))
* tidying ([68472dd](https://gitlab.com/gotools-land/go-builder-generator/commit/68472dd025a14fd6ec3943bd0787a6f8eecce2a2))
* update some code comments ([8339ebe](https://gitlab.com/gotools-land/go-builder-generator/commit/8339ebe2efc3dc996b3fe4b518c85f63afdb8b55))

## [1.0.0-alpha.13](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.12...v1.0.0-alpha.13) (2024-02-11)


### Features

* add --no-notice option on generate command to remove code generated notice ([56a5abd](https://gitlab.com/gotools-land/go-builder-generator/commit/56a5abd5a7427be610e9a21822737b9e4c40d66c))
* add anonymous interface and anonymous struct builder generation ([56c863f](https://gitlab.com/gotools-land/go-builder-generator/commit/56c863fe27bc61651b6117c7a4df0fbdf0db7547))


### Bug Fixes

* ensure acronyms keeps their uppercase format ([1ea66e2](https://gitlab.com/gotools-land/go-builder-generator/commit/1ea66e2b7ca2521697b52ab33c699d1485922dab))

## [1.0.0-alpha.12](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.11...v1.0.0-alpha.12) (2024-02-08)


### Features

* **generate:** add sort to struct properties alphabetically ([b744439](https://gitlab.com/gotools-land/go-builder-generator/commit/b7444399b20eaee54906f74ba4e9c35ab6ff6a01))

## [1.0.0-alpha.11](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.10...v1.0.0-alpha.11) (2024-02-08)


### Bug Fixes

* bad errors package import ([de53ca5](https://gitlab.com/gotools-land/go-builder-generator/commit/de53ca5b2644bd83ce570a329f8f148e6dddf843))


### Chores

* **go:** upgrade to go1.22 ([4c8acc8](https://gitlab.com/gotools-land/go-builder-generator/commit/4c8acc82a5aceb0135667e41ca1f2a73b68dcf9a))

## [1.0.0-alpha.10](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.9...v1.0.0-alpha.10) (2024-02-08)


### Bug Fixes

* **generate:** linting issue ([5fa5a15](https://gitlab.com/gotools-land/go-builder-generator/commit/5fa5a1530987038fc63eaddeea9f3fa27860a844))


### Code Refactoring

* rework structs generation to avoid to many inspection of given file ([2fea05e](https://gitlab.com/gotools-land/go-builder-generator/commit/2fea05eefa455308454cfea5a702941709142ad8))

## [1.0.0-alpha.9](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.8...v1.0.0-alpha.9) (2024-02-06)


### Features

* **prefixer:** add Valid function and as such avoid panics in code in favor of CLI fatal at the end of execution with proper error ([cfdf137](https://gitlab.com/gotools-land/go-builder-generator/commit/cfdf13734558c563e76d8b42306bd5d056388bb3))

## [1.0.0-alpha.8](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.7...v1.0.0-alpha.8) (2024-02-06)


### Bug Fixes

* linting issues ([2583544](https://gitlab.com/gotools-land/go-builder-generator/commit/2583544a0dae95d301bc383dfe9c7ce41bebdf65))


### Code Refactoring

* use ast.Inspect instead of Walk to handle parsing errors ([42cf4e6](https://gitlab.com/gotools-land/go-builder-generator/commit/42cf4e66e1188f120cbc3ce43d56768f68a6fc42))

## [1.0.0-alpha.7](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.6...v1.0.0-alpha.7) (2024-02-06)


### Chores

* add link in generated code to gitlab project ([cf6afe2](https://gitlab.com/gotools-land/go-builder-generator/commit/cf6afe2b8838c5c37c4ff06c69ccc006a5a2233f))


### Code Refactoring

* **generate:** change how _impl file creation condition is handled ([9ecc316](https://gitlab.com/gotools-land/go-builder-generator/commit/9ecc3161be726b6b850154e42d2ffa2a126b3f44))

## [1.0.0-alpha.6](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.5...v1.0.0-alpha.6) (2024-02-05)


### Bug Fixes

* **generate:** generate _impl file only if one of the builders has default functions ([19fde1c](https://gitlab.com/gotools-land/go-builder-generator/commit/19fde1cc5c161dae88c00ffbcd555493436db021))
* **prefixer:** add any as primitive type ([80140b4](https://gitlab.com/gotools-land/go-builder-generator/commit/80140b4e65b9ada351252ea8707940ceeda0b252))

## [1.0.0-alpha.5](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.4...v1.0.0-alpha.5) (2024-02-04)


### Bug Fixes

* **options:** builder tag not being here failing other options ([a3f630a](https://gitlab.com/gotools-land/go-builder-generator/commit/a3f630a623020e18095ff437076ed617bac345af))


### Documentation

* add examples and minimal README.md ([9dea7be](https://gitlab.com/gotools-land/go-builder-generator/commit/9dea7beee08151df9277078683b53b76d308598d))


### Chores

* tidying ([33ea113](https://gitlab.com/gotools-land/go-builder-generator/commit/33ea11373adc508300f8b99707d176fb6d276c90))

## [1.0.0-alpha.4](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.3...v1.0.0-alpha.4) (2024-02-04)


### Chores

* **options:** remove unused default option ([b96c7cf](https://gitlab.com/gotools-land/go-builder-generator/commit/b96c7cf6c47c4487860d8c50fb4a02ca917b8dc3))


### Code Refactoring

* **gen:** rework Build function in _gen template ([1387ad4](https://gitlab.com/gotools-land/go-builder-generator/commit/1387ad46f357a3c3d7b43901452a30805635ffca))

## [1.0.0-alpha.3](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.2...v1.0.0-alpha.3) (2024-02-04)


### Features

* add append option and better handling of options overall ([c007ee2](https://gitlab.com/gotools-land/go-builder-generator/commit/c007ee2895174dad113245abefde9b14259647e1))


### Bug Fixes

* linting issues ([cc2ab58](https://gitlab.com/gotools-land/go-builder-generator/commit/cc2ab5859d6c5bf6e4437fa3cf4fde929cbca32f))


### Code Refactoring

* **prefixer:** handle prefixer with duck typing and as such handle composition and unexported types or fields ([e36eff8](https://gitlab.com/gotools-land/go-builder-generator/commit/e36eff8c96f9b83806c5879e186ec0837388219d))
* use options struct for fields builder tag options ([6b22f8b](https://gitlab.com/gotools-land/go-builder-generator/commit/6b22f8bd7680a201fbb2e3a62ddd7e5a2683fe05))
* use standard functions to remove dependencies ([8d3b6a9](https://gitlab.com/gotools-land/go-builder-generator/commit/8d3b6a9d2df59179d6e28c5fd1060c2d9bcbd9fa))

## [1.0.0-alpha.2](https://gitlab.com/gotools-land/go-builder-generator/compare/v1.0.0-alpha.1...v1.0.0-alpha.2) (2024-02-02)


### Features

* add default_func feature to generate in another file functions that will be called on Build to set default fields ([1a15ef6](https://gitlab.com/gotools-land/go-builder-generator/commit/1a15ef68c9b7b849d62791f69f086b9c2221075e))

## 1.0.0-alpha.1 (2024-02-01)


### Features

* create go builder generator ([3dc5389](https://gitlab.com/gotools-land/go-builder-generator/commit/3dc53899cd8ddb1a0e41147c5b4c39c944fde79e))


### Bug Fixes

* tyding and bad imports ([961caf6](https://gitlab.com/gotools-land/go-builder-generator/commit/961caf6033c7232ebdbfec2040fa02aa4213f672))


### Chores

* add LICENSE ([65f8dd5](https://gitlab.com/gotools-land/go-builder-generator/commit/65f8dd5fb4a3975b5dd53fe310d569a63464eb27))
* init project with README ([af75f31](https://gitlab.com/gotools-land/go-builder-generator/commit/af75f31977d8730ef509090e29990373dee21a20))
