package main

import "gitlab.com/gotools-land/go-builder-generator/internal/cobra"

func main() {
	cobra.Execute()
}
